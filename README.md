# Shix Tools

Tools for setting up and configuring SHIX

# Join the Discord server!

[![Discord](https://discordapp.com/api/guilds/974345893723648061/widget.png?style=banner4)](https://discord.gg/rk8SD7ejGr)


## Setup Database

Install python requirements
Copy models.py from api-server into the root directory of the tool
Set database creds with the env var `SHIX_API_DB_URL`
Run database-setup.py